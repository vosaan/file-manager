<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FolderDescription
 *
 * @property int $id
 * @property string $folder_path
 * @property string $folder_description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */

class FolderDescription extends Model
{
    protected $table = 'folders_descriptions';

    protected $fillable = [
        'folder_path',
        'folder_description'
    ];
}
