<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\FolderDescription;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FolderDescriptionController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getFolderDescriptionByFolderPath(Request $request): JsonResponse
    {
        /* @var $folderDescriptionModel FolderDescription */

        $folderPath = $request->get('folder_path');

        $folderDescription = null;

        $folderDescriptionModel = $this->getFolderDescriptionModel($folderPath);

        if ($folderDescriptionModel) {
            $folderDescription = $folderDescriptionModel->folder_description;
        }

        return response()->json(['data' => [
            'folder_description' => $folderDescription
        ]], Response::HTTP_OK);

    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function setFolderDescription(Request $request): JsonResponse
    {
        /* @var $folderDescription FolderDescription */

        $folderPath = $request->get('folder_path');

        $folderDescriptionModel = $this->getFolderDescriptionModel($folderPath);

        if ($folderDescriptionModel) {
            $folderDescriptionModel->update([
                'folder_description' => $request->get('folder_description')
            ]);
        } else {
            FolderDescription::query()->create([
                'folder_path' => $request->get('folder_path'),
                'folder_description' => $request->get('folder_description')
            ]);
        }

        return response()->json(['data' => 'success'], Response::HTTP_OK);
    }

    /**
     * @param string $folderPath
     * @return FolderDescription|null
     */
    private function getFolderDescriptionModel(string $folderPath): ?FolderDescription
    {
        return FolderDescription::where('folder_path', '=', $folderPath)->first();
    }
}
