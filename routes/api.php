<?php

use App\Http\Controllers\Api\FolderDescriptionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('get-folder-description', [FolderDescriptionController::class, 'getFolderDescriptionByFolderPath'])
    ->name('get-folder-description');

Route::post('set-folder-description', [FolderDescriptionController::class, 'setFolderDescription'])
    ->name('set-folder-description');
