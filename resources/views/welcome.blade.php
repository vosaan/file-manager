<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>File Manager</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

</head>
<body class="antialiased">
<div id="app-body">
    <file-manager
        get-folder-description-url="{{ route('get-folder-description') }}"
        set-folder-description-url="{{ route('set-folder-description') }}"
    ></file-manager>
</div>
</body>
<script src="{{ asset('/js/app.js') }}"></script>
</html>
