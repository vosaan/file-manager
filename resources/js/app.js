require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';
import FileManager from './components/file-manager/init'

Vue.use(Vuex);

// create Vuex store, if you don't have it
const store = new Vuex.Store();

Vue.use(FileManager, {store});

const app = new Vue({
    el: '#app-body',
    store
});
